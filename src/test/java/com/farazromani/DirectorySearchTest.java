package com.farazromani;

import com.farazromani.TestHelper.TestHelperUtil;
import com.farazromani.Utils.DirectorySearch;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.rauschig.jarchivelib.ArchiveFormat;
import org.rauschig.jarchivelib.Archiver;
import org.rauschig.jarchivelib.ArchiverFactory;
import org.rauschig.jarchivelib.CompressionType;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.LinkedList;

public class DirectorySearchTest {

    /*
    testDir
    |-  testDir01
    |-  testDir02
        |-testDir0201
     */

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    private final String TEST_DIR_PARENT = "." + File.separator + "testDir_" + System.currentTimeMillis();
    private final String TEST_DIR_PATH_1 = TEST_DIR_PARENT + File.separator + "testDir01_" + System.currentTimeMillis();
    private final String TEST_DIR_PATH_2 = TEST_DIR_PARENT + File.separator + "testDir02_" + System.currentTimeMillis();
    private final String TEST_DIR_PATH_2_1 = TEST_DIR_PATH_2 + File.separator + "testDir0201_" + System.currentTimeMillis();
    private final String TEST_EMPTY_DIR = TEST_DIR_PARENT + File.separator + "testDirEmpty_" + System.currentTimeMillis();

    private File testDirParent;
    private File testDir01;
    private File testDir02;
    private File testDir0201;
    private File testEmptyDir;

    private File file01;
    private File file02;
    private File file03;

    @Before
    public void createDirAndFile() {
        testDirParent = new File(TEST_DIR_PARENT);

        testDir01 = new File(TEST_DIR_PATH_1);
        testDir01.mkdirs();

        testDir02 = new File(TEST_DIR_PATH_2);
        testDir02.mkdirs();

        testDir0201 = new File(TEST_DIR_PATH_2_1);
        testDir0201.mkdirs();

        testEmptyDir = new File(TEST_EMPTY_DIR);
        testEmptyDir.mkdirs();
    }

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    @After
    public void deleteDirAndFiles() throws IOException {
        FileUtils.deleteDirectory(testDirParent);
    }

    @Test
    public void testDirectorySearchWithFiles() {
        // create and insert three files
        TestHelperUtil.createFile("file01", TestHelperUtil.generateRandomString(5), testDir01);
        TestHelperUtil.createFile("file02", TestHelperUtil.generateRandomString(5), testDir02);
        TestHelperUtil.createFile("file03", TestHelperUtil.generateRandomString(5), testDir0201);

        // search dir
        CountedFileContainer fileContainer = DirectorySearch.Search(testDirParent);

        Assert.assertNotNull(fileContainer);

        LinkedList<CountedFile> countedFiles = fileContainer.getList();
        Assert.assertNotNull(countedFiles);
        Assert.assertEquals(countedFiles.size(), 3);

        String actual = outContent.toString().replace("\r", "");
        String expected = "3 total number of text files discovered.\n" +
                "5 total number of directories discovered (incl. root).\n" +
                "\n";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDirectorySearchNoFiles() {
        // search dir
        CountedFileContainer fileContainer = DirectorySearch.Search(testEmptyDir);

        Assert.assertNotNull(fileContainer);

        LinkedList<CountedFile> countedFiles = fileContainer.getList();
        Assert.assertNotNull(countedFiles);
        Assert.assertTrue(countedFiles.isEmpty());

        String actual = outContent.toString().replace("\r", "");
        String expected = "0 total number of text files discovered.\n" +
                "1 total number of directories discovered (incl. root).\n" +
                "\n";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDirectorySearchCompressedFile() throws IOException {
        // name of unarchived dir
        String archiveName = "archive";

        // create file to compress within testDir
        File src = TestHelperUtil.createFile("foobar.txt", "this is a test", testDirParent);

        // create compressed file
        Archiver archiver = ArchiverFactory.createArchiver(ArchiveFormat.TAR, CompressionType.GZIP);
        archiver.create(archiveName, testDirParent, src);

        CountedFileContainer fileContainer = DirectorySearch.Search(testDirParent);
        Assert.assertNotNull(fileContainer);

        LinkedList<CountedFile> countedFiles = fileContainer.getList();
        Assert.assertNotNull(countedFiles);

        String actual = outContent.toString().replace("\r", "");
        String expected = "2 total number of text files discovered.\n" +
                "6 total number of directories discovered (incl. root).\n" +
                "\n";
        Assert.assertEquals(expected, actual);

        // check if decompressed file was added to container
        boolean hasFoobarFile = false;
        for (CountedFile countedFile : countedFiles) {
            String filename = countedFile.getFile().getAbsolutePath();
            if (filename.contains("archive_decompressed" + File.separator + "foobar.txt") && !hasFoobarFile) {
                hasFoobarFile = true;
            }
        }

        Assert.assertTrue(hasFoobarFile);
    }
}
