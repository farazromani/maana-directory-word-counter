package com.farazromani;

import com.farazromani.Utils.ExceptionHandlerUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Will represent the Text file. It will also hold the total number of words within the file.
 */
public class CountedFile {
    private int wordCount;
    private File file;
    private File actualFile;

    public CountedFile(File file) {
        this.file = file;
        wordCount = 0;
        actualFile = null;

        countWords();
    }

    /**
     * Count the number of words within the given file. The algorithm will iterate through each line of the file and
     * split upon non-word characters, such as spaces and other forms of abbreviations and markings. The
     * @code{wordCount} object will be incremented.
     */
    private void countWords() {
        if (file == null) return;

        // Regex to find non-alphanumerical chars. Rejects hyphenated words
        final String REGEX = "(?=\\S*[^'-])([^a-zA-Z0-9'-]+)";

        try {
            Scanner input = new Scanner(file);
            while (input.hasNextLine()) {
                String nextLine = input.nextLine();
                String nextLineTrim = nextLine.trim();

                if (nextLineTrim.length() != 0) {
                    String[] split = nextLineTrim.split(REGEX);
                    wordCount += split.length;
                }

            }

            input.close();
        }

        //
        // Handle errors by printing appropriate message and exiting program
        //
        catch (FileNotFoundException e) {
            ExceptionHandlerUtil.HandleException(e, file);
        }

    }

    public int getWordCount() {
        return wordCount;
    }

    public File getFile() {
        return file;
    }

}
