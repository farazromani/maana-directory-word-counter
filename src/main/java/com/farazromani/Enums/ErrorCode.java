package com.farazromani.Enums;

public enum ErrorCode {
    NO_TEXT_FILES_PARSED("No text files found or any with words"),
    INVALID_PATH("Invalid path"),
    MISSING_PATH_ENV("Missing file path in command line argument"),
    ACCESS_DENIED("Access denied"),
    UNKNOWN_PARSING_ERROR("Unknown parsing error");

    private final String errorCode;

    ErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String toString() {
        return this.errorCode;
    }
}
