package com.farazromani;

import com.farazromani.TestHelper.TestHelperUtil;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class CounterFileContainerTest {

    private final String TEST_DIR_PARENT = "." + File.separator + "testDir_" + System.currentTimeMillis();
    private File testDirParent;

    @Before
    public void createDirAndFile() {
        testDirParent = new File(TEST_DIR_PARENT);
        testDirParent.mkdirs();
    }

    @After
    public void deleteTestDir() throws IOException {
        FileUtils.deleteDirectory(testDirParent);
    }

    @Test
    public void countedFileContainerTest() {
        final int MAX_COUNT = 10;
        final int MIN_COUNT = 3;

        CountedFileContainer container = new CountedFileContainer();
        Assert.assertNotNull(container);
        Assert.assertNotNull(container.getList());
        Assert.assertEquals(container.getMaxWordCount(), Integer.MIN_VALUE);
        Assert.assertEquals(container.getMinWordCount(), Integer.MAX_VALUE);
        Assert.assertEquals(container.getMaxAbsolutePathLen(), 0);

        // add files
        File maxFile = TestHelperUtil.createFile("maxFile", TestHelperUtil.generateRandomString(MAX_COUNT), testDirParent);
        File minFile = TestHelperUtil.createFile("minFile", TestHelperUtil.generateRandomString(MIN_COUNT), testDirParent);
        File ranFile = TestHelperUtil.createFile("ranFile", TestHelperUtil.generateRandomString(5), testDirParent);
        container.add(new CountedFile(maxFile));
        container.add(new CountedFile(minFile));
        container.add(new CountedFile(ranFile));

        // test class objects after adding files
        Assert.assertNotNull(container);

        LinkedList<CountedFile> list = container.getList();
        Assert.assertNotNull(list);
        Assert.assertEquals(list.size(), 3);
        Assert.assertEquals(container.getMaxAbsolutePathLen(), maxFile.getAbsolutePath().length());
        Assert.assertEquals(container.getMaxWordCount(), MAX_COUNT);
        Assert.assertEquals(container.getMinWordCount(), MIN_COUNT);
    }
}
