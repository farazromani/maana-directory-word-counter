package com.farazromani.Utils;

import com.farazromani.Enums.Extension;
import org.rauschig.jarchivelib.Archiver;
import org.rauschig.jarchivelib.ArchiverFactory;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * This Singleton method will decompress directories that have been compressed using common methods -- ZIP, TAR, and TAR
 * GZ. The main method will then return a File object representing the decompressed input object.
 */
public class DecompressUtil {
    private static final String ZIP_EXT = Extension.ZIP.toString();
    private static final String TAR_EXT = Extension.TAR.toString();
    private static final String TAR_GZ_EXT = Extension.TAR_GZ.toString();

    private static DecompressUtil ourInstance = new DecompressUtil();

    private DecompressUtil() {
    }

    public static DecompressUtil getInstance() {
        return ourInstance;
    }

    /**
     * Unzip @code{compressedFile} if it is .zip or .tar.gz type.
     *
     * @param compressedFile
     *         zip compressedFile to Decompress
     * @return MyFile object representing unzipped directory if Decompress successful; otherwise, NULL.
     */
    public static File Decompress(File compressedFile) {
        // Null check
        if (compressedFile == null || !compressedFile.exists()) return null;

        File dest = null;
        try {
            String uniqueDirName = getUniqueDirName(compressedFile);

            if (uniqueDirName == null) return null;

            dest = new File(uniqueDirName);
            Archiver archiver = ArchiverFactory.createArchiver(compressedFile);
            archiver.extract(compressedFile, dest);
        }

        //
        // Handle errors by printing appropriate message and exiting program
        //
        catch (IOException e) {
            ExceptionHandlerUtil.HandleException(e, compressedFile);
        }

        return dest;
    }

    /**
     * Will create a unique directory name if directory name is already taking. This is to avoid overriding when
     * compressed file is decompressed into same directory.
     *
     * @param file
     * @return
     */
    private static String getUniqueDirName(File file) {
        if (file == null) return null;

        String fileName = file.getName();

        // strip extension
        fileName = stripExt(fileName);

        // check if fileName exist in parent directory
        boolean fileNameExists = dirHasSameName(file.getParentFile(), fileName);
        fileName = (fileNameExists)
                ? fileName + "_" + System.currentTimeMillis()
                : fileName;

        return file.getParent() + File.separator + fileName + "_decompressed";
    }

    /**
     * Checks @code{parentFile} for directories with same name as @code{toFind}
     *
     * @param parentFile
     *         directory to search for same named directory as @code{toFind}
     * @param toFind
     *         directory name to find in list of @code{parentFile}'s directories. This is case-sensitive.
     * @return TRUE if parent directory contains a directory with same name is @code{toFind}; FALSE otherwise
     */
    private static boolean dirHasSameName(File parentFile, String toFind) {
        if (parentFile == null || toFind == null || toFind.isEmpty()) return false;

        File[] list = parentFile.listFiles();

        if (list != null) {
            List<File> fileNames = Arrays.asList(list);

            for (File file : fileNames) {

                // only check directories' name
                if (file.isDirectory() && file.getName().equals(toFind)) {
                    return true;
                }
            }

        }

        return false;
    }

    /**
     * Removes common compression extensions and returns substring
     *
     * @param fileName
     *         object to strip extension from
     * @return substring for filename without compression-type extension
     */
    private static String stripExt(String fileName) {
        if (fileName.endsWith(ZIP_EXT)) {
            return fileName.substring(0, fileName.lastIndexOf(ZIP_EXT));
        }

        if (fileName.endsWith(TAR_GZ_EXT)) {
            return fileName.substring(0, fileName.lastIndexOf(TAR_GZ_EXT));
        }

        if (fileName.endsWith(TAR_EXT)) {
            return fileName.substring(0, fileName.lastIndexOf(TAR_EXT));
        }

        return fileName;
    }
}
