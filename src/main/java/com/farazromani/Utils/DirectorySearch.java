package com.farazromani.Utils;

import com.farazromani.CountedFile;
import com.farazromani.CountedFileContainer;
import com.farazromani.Enums.Extension;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Will recursively search directories for text and compressed files. If a TXT file is discovered, it will call another
 * method to count the words within. If a compressed file is found, it will call the appropriate class/method to
 * decompress. After which, it will continue it's recursive search within the newly decompressed directory.
 * <p>
 * In the end, the class's main method will return a container containing ContentFile, which represents text files whose
 * words have been counted.
 */
public class DirectorySearch {
    private static DirectorySearch ourInstance = new DirectorySearch();

    private DirectorySearch() {
    }

    public static DirectorySearch getInstance() {
        return ourInstance;
    }


    /**
     * Recursively search directories to find *.txt and compressed files
     *
     * @param startDir parent directory to recurse
     * @return container containing CountedFile objects
     */
    public static CountedFileContainer Search(File startDir) {
        // track how many directories encountered
        List<String> dirs = new LinkedList<String>();
        CountedFileContainer fileContainer = new CountedFileContainer();

        search(startDir, fileContainer, dirs);

        //
        // Output search results
        //
        LinkedList<CountedFile> countedFiles = fileContainer.getList();
        System.out.println((countedFiles == null) ? 0 : countedFiles.size() + " total number of text files discovered.");
        System.out.println(dirs.size() + " total number of directories discovered (incl. root).\n");

        return fileContainer;
    }

    /**
     * Helper method to recursively search from @code{file} to find text files to count words.
     *
     * @param file          current File object to handle as either text, compressed, or dir
     * @param fileContainer container to add CountedFile objects
     */
    private static void search(File file, CountedFileContainer fileContainer, List<String> dirs) {
        if (file.isHidden()) return;
        if (file.isFile()) {

            String fileName = file.getName();

            // check if file is compressed
            boolean isCompressExt = fileName.endsWith(Extension.ZIP.toString())
                    || fileName.endsWith(Extension.TAR_GZ.toString())
                    || fileName.endsWith(Extension.TAR.toString());

            //
            // Handle compressed files
            //
            if (isCompressExt) {
                File unzipFile = DecompressUtil.Decompress(file);

                // handle unknown exception
                if (unzipFile == null) {
                    ExceptionHandlerUtil.UnknownError();
                }

                // continue into Decompress file's directory
                else {
                    search(unzipFile, fileContainer, dirs);
                }
            }

            //
            // Handle text files
            //
            else if (fileName.endsWith(Extension.TXT.toString())) {
                CountedFile countedFile = new CountedFile(file);

                // only add Files that contains words
                if (countedFile.getWordCount() > 0) {
                    fileContainer.add(countedFile);
                }
            }
        }

        //
        // 'file' obj was not file type, is directory so continue traversal
        //
        else if (file.isDirectory()) {
            dirs.add(file.getAbsolutePath());

            File[] files = file.listFiles();

            // if null, then cannot access directory / permission denied
            if (files == null) {
                ExceptionHandlerUtil.AccessDenied(file);
            }

            // continue directory traversal
            else {
                for (File f : files) {
                    search(f, fileContainer, dirs);
                }
            }
        }
    }
}
