package com.farazromani.Enums;

public enum Extension {
    TXT(".txt"),
    ZIP(".zip"),
    TAR(".tar"),
    TAR_GZ(".tar.gz");

    private final String extension;

    private Extension(String extension) {
        this.extension = extension;
    }

    @Override
    public String toString() {
        return this.extension;
    }
}
