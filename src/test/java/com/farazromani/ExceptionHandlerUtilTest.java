package com.farazromani;

import com.farazromani.Enums.ErrorCode;
import com.farazromani.Utils.ExceptionHandlerUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class ExceptionHandlerUtilTest {

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    @Test
    public void testHandleException() {
        exit.expectSystemExitWithStatus(-1);

        String tmpFilename = "some-random-file";
        File tmpFile = new File(tmpFilename);

        ExceptionHandlerUtil.HandleException(new RuntimeException("Foobar"), tmpFile);
        String expected = "ERROR\nREASON:\tUnknown parsing error\n";
        String actual = outContent.toString().replace("\r", "");
        assertEquals(expected, actual);
    }

    @Test
    public void testInvalidArguments() {
        exit.expectSystemExitWithStatus(-1);

        ExceptionHandlerUtil.InvalidArguments();
        String expected = "ERROR\nREASON:\t" + ErrorCode.MISSING_PATH_ENV + "\n";
        String actual = outContent.toString().replace("\r", "");
        assertEquals(expected, actual);
    }

    @Test
    public void testNoSuchDirectory() {
        exit.expectSystemExitWithStatus(-1);

        String invalidFilename = "i-dont-exist";

        File invalidFile = new File(invalidFilename);
        File validFile = new File(".");

        String cleanParentAbsolutePath = validFile.getAbsolutePath().substring(0, validFile.getAbsolutePath().lastIndexOf('.'));
        String invalidFilepath = cleanParentAbsolutePath + invalidFilename;

        ExceptionHandlerUtil.NoSuchDirectory(invalidFile);
        String expected = "ERROR:\tCannot process '" + invalidFilename + "' (" + invalidFilepath + ")\nREASON:\t" + ErrorCode.INVALID_PATH + "\n";
        String actual = outContent.toString().replace("\r", "");
        assertEquals(expected, actual);
    }

    @Test
    public void testNoTextFiles() {
        ExceptionHandlerUtil.NoTextFiles();
        String expected = "ERROR\nREASON:\t" + ErrorCode.NO_TEXT_FILES_PARSED + "\n";
        String actual = outContent.toString().replace("\r", "");
        assertEquals(expected, actual);
    }

    @Test
    public void testAccessDenied() {
        exit.expectSystemExitWithStatus(-1);

        String restrictedFilename = "i-am-restricted";

        File restrictedFile = new File(restrictedFilename);
        File validFile = new File(".");

        String cleanParentAbsolutePath = validFile.getAbsolutePath().substring(0, validFile.getAbsolutePath().lastIndexOf('.'));
        String restrictedFilepath = cleanParentAbsolutePath + restrictedFilename;

        ExceptionHandlerUtil.AccessDenied(restrictedFile);
        String expected = "ERROR:\tCannot process '" + restrictedFilename + "' (" + restrictedFilepath + ")\nREASON:\t" + ErrorCode.ACCESS_DENIED + "\n";
        String actual = outContent.toString().replace("\r", "");
        assertEquals(expected, actual);
    }

    @Test
    public void testUnknownError() {
        exit.expectSystemExitWithStatus(-1);

        ExceptionHandlerUtil.UnknownError();
        String expected = "ERROR\nREASON:\tUnknown parsing error\n";
        String actual = outContent.toString().replace("\r", "");
        assertEquals(expected, actual);
    }
}