package com.farazromani;

import com.farazromani.TestHelper.TestHelperUtil;
import org.apache.commons.io.FileUtils;
import org.junit.*;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class MainTest {

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    private final String TEST_DIR_PARENT = "." + File.separator + "testDir_" + System.currentTimeMillis();
    private File testDirParent;

    @Before
    public void createDirAndFile() {
        testDirParent = new File(TEST_DIR_PARENT);
        testDirParent.mkdirs();
    }

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void deleteTestDir() throws IOException {
        FileUtils.deleteDirectory(testDirParent);
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    @Test
    public void mainNoArgsTest() throws IOException {
        exit.expectSystemExitWithStatus(-1);
        Main.main(null);
    }

    @Test
    public void mainInvalidPathTest() throws IOException {
        exit.expectSystemExitWithStatus(-1);
        String[] args = {"foobar123"};
        Main.main(args);
    }

    @Test
    public void mainHappyPathTest() throws IOException {
        int numOfFiles = 3;
        while (numOfFiles > 0) {
            TestHelperUtil.createFile(
                    TestHelperUtil.generateWord(5) + ".txt",
                    TestHelperUtil.generateRandomString(numOfFiles),
                    testDirParent
            );
            numOfFiles--;
        }

        String[] args = {testDirParent.getAbsolutePath()};
        Main.main(args);

        String expected = "3 total number of text files discovered.\n" +
                "1 total number of directories discovered (incl. root).\n" +
                "\n" +
                "    3        -                                        \n" +
                "    2        -                                        \n" +
                "    1        -                                        \n" +
                "    0        -        -        -        -        -    \n" +
                "           [1-6]   (6-12]   (12-18]  (18-24]  (24-30] ";
        String actual = outContent.toString().replace("\r", "");
        Assert.assertTrue(actual.contains(expected));

    }
}
