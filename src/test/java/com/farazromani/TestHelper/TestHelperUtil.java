package com.farazromani.TestHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Random;

public class TestHelperUtil {
    private static TestHelperUtil ourInstance = new TestHelperUtil();

    private TestHelperUtil() {
    }

    public static TestHelperUtil getInstance() {
        return ourInstance;
    }

    public static File createFile(String filename, String content, File dstDir) {

        // add missing '.txt' extension;
        if (!filename.endsWith(".txt")) {
            filename = filename + ".txt";
        }

        try {
            PrintWriter writer = new PrintWriter(dstDir + File.separator + filename, "UTF-8");
            writer.println(content);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return new File(dstDir + File.separator + filename);
    }

    public static String generateRandomString(int numOfWords) {
        StringBuilder sb = new StringBuilder();

        while (numOfWords-- > 0) {
            int len = generateRandomNum(3, 13);
            String word = generateWord(len);
            sb.append(word).append(' ');
        }

        return sb.toString();
    }

    public static String generateWord(int wordLen) {
        StringBuilder sb = new StringBuilder();

        while (wordLen-- > 0) {
            int index = generateRandomNum(((int) 'a'), ((int) 'z'));
            sb.append((char) index);
        }

        return sb.toString();
    }

    private static int generateRandomNum(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }
}
