package com.farazromani;

import com.farazromani.Utils.ExceptionHandlerUtil;
import com.farazromani.Utils.HistogramUtil;

import java.util.LinkedList;

/**
 * Linked List backed container to hold each CountedFile object. This class will also maintain the min/max word count,
 * which will be used to produce the histogram.
 */
public class CountedFileContainer {
    private int minWordCount;
    private int maxWordCount;
    private int maxAbsolutePathLen;
    private LinkedList<CountedFile> list;

    public CountedFileContainer() {
        minWordCount = Integer.MAX_VALUE;
        maxWordCount = Integer.MIN_VALUE;
        list = new LinkedList<CountedFile>();
    }

    /**
     * Add CountedFile object to linked list.
     *
     * @param countedFile
     *         obj to add to list
     * @return TRUE if addition was successful; FALSE otherwise
     */
    public boolean add(CountedFile countedFile) {
        boolean success = list.add(countedFile);
        updateMinMax(countedFile);
        return success;
    }

    /**
     * Update min/max objs
     *
     * @param countedFile
     *         CountedFile obj to update min/max values against
     */
    private void updateMinMax(CountedFile countedFile) {
        int wordCount = countedFile.getWordCount();
        minWordCount = Math.min(minWordCount, wordCount);
        maxWordCount = Math.max(maxWordCount, wordCount);
        maxAbsolutePathLen = Math.max(maxAbsolutePathLen, countedFile.getFile().getAbsolutePath().length());
    }

    public int getMinWordCount() {
        return minWordCount;
    }

    public int getMaxWordCount() {
        return maxWordCount;
    }

    public int getMaxAbsolutePathLen() {
        return maxAbsolutePathLen;
    }

    public LinkedList<CountedFile> getList() {
        return list;
    }

    /**
     * Will call the Histogram utility to output a histogram representation of the CountedFiles in the list.
     */
    void printHistogram() {
        if (list == null || list.isEmpty()) {
            ExceptionHandlerUtil.NoTextFiles();
        } else {
            HistogramUtil.PrintHistogram(this);
        }
    }
}
