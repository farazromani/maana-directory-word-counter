package com.farazromani;

import com.farazromani.TestHelper.TestHelperUtil;
import com.farazromani.Utils.DecompressUtil;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.rauschig.jarchivelib.ArchiveFormat;
import org.rauschig.jarchivelib.Archiver;
import org.rauschig.jarchivelib.ArchiverFactory;
import org.rauschig.jarchivelib.CompressionType;

import java.io.File;
import java.io.IOException;

public class DecompressUtilTest {

    // TODO: Test hidden files

    private final String TEST_DIR_PATH = "." + File.separator + "testDir" + System.currentTimeMillis();
    private File testDir;

    @Before
    public void createTestDirs() {
        testDir = new File(TEST_DIR_PATH);
        testDir.mkdirs();
    }

    @After
    public void deleteTestDir() throws IOException {
        FileUtils.deleteDirectory(testDir);
    }

    @Test
    public void testDecompress() throws IOException {
        // name of unarchived dir
        String archiveName = "archive";

        // create file to compress within testDir
        File src =  TestHelperUtil.createFile("foobar.txt", "this is a test", testDir);

        // create compressed file
        Archiver archiver = ArchiverFactory.createArchiver(ArchiveFormat.TAR, CompressionType.GZIP);
        File archive = archiver.create(archiveName, testDir, src);

        // test Decompress method
        File decompressed = DecompressUtil.Decompress(archive);

        Assert.assertEquals(archiveName + "_decompressed", decompressed.getName());
        Assert.assertNotNull(decompressed);
        Assert.assertTrue(decompressed.isDirectory());
        Assert.assertTrue(decompressed.exists());
        Assert.assertFalse(decompressed.isHidden());
    }


}
