package com.farazromani;

import com.farazromani.TestHelper.TestHelperUtil;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class HistogramUtilTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    private final String TEST_DIR_PARENT = "." + File.separator + "testDir_" + System.currentTimeMillis();
    private File testDirParent;

    @Before
    public void createDirAndFile() {
        testDirParent = new File(TEST_DIR_PARENT);
        testDirParent.mkdirs();
    }

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    @After
    public void deleteTestDir() throws IOException {
        FileUtils.deleteDirectory(testDirParent);
    }

    @Test
    public void printHistogramTestLessThanTenFiles() {
        CountedFileContainer container = new CountedFileContainer();

        int numOfFiles = 3;
        while (numOfFiles > 0) {
            File file = TestHelperUtil.createFile(
                    TestHelperUtil.generateWord(5) + ".txt",
                    TestHelperUtil.generateRandomString(numOfFiles),
                    testDirParent
            );
            container.add(new CountedFile(file));
            numOfFiles--;
        }

        container.printHistogram();

        String expected = "    3        -                                        \n" +
                "    2        -                                        \n" +
                "    1        -                                        \n" +
                "    0        -        -        -        -        -    \n" +
                "           [1-6]   (6-12]   (12-18]  (18-24]  (24-30] ";
        String actual = outContent.toString().replace("\r", "");
        Assert.assertTrue(actual.contains(expected));
    }

    @Test
    public void printHistogramTestGreaterThanTenFiles() {
        CountedFileContainer container = new CountedFileContainer();

        int numOfFiles = 150;
        while (numOfFiles > 0) {
            File file = TestHelperUtil.createFile(
                    TestHelperUtil.generateWord(5) + ".txt",
                    TestHelperUtil.generateRandomString(numOfFiles),
                    testDirParent
            );
            container.add(new CountedFile(file));
            numOfFiles--;
        }

        container.printHistogram();

        String expected = "    26          -          -          -          -          -                \n" +
                "    24          -          -          -          -          -                \n" +
                "    21          -          -          -          -          -                \n" +
                "    19          -          -          -          -          -          -     \n" +
                "    16          -          -          -          -          -          -     \n" +
                "    13          -          -          -          -          -          -     \n" +
                "    11          -          -          -          -          -          -     \n" +
                "     8          -          -          -          -          -          -     \n" +
                "     6          -          -          -          -          -          -     \n" +
                "     3          -          -          -          -          -          -     \n" +
                "     0          -          -          -          -          -          -     \n" +
                "             [1-26]     (26-52]    (52-78]   (78-104]   (104-130]  (130-156] ";
        String actual = outContent.toString().replace("\r", "");
        Assert.assertTrue(actual.contains(expected));
    }

    @Test
    public void printHistogramTestTenFiles() {
        CountedFileContainer container = new CountedFileContainer();

        int numOfFiles = 10;
        while (numOfFiles > 0) {
            File file = TestHelperUtil.createFile(
                    TestHelperUtil.generateWord(5) + ".txt",
                    TestHelperUtil.generateRandomString(numOfFiles),
                    testDirParent
            );
            container.add(new CountedFile(file));
            numOfFiles--;
        }

        container.printHistogram();

        String expected = "    10          -                                                                                                        \n" +
                "     9          -                                                                                                        \n" +
                "     8          -                                                                                                        \n" +
                "     7          -                                                                                                        \n" +
                "     6          -                                                                                                        \n" +
                "     5          -                                                                                                        \n" +
                "     4          -                                                                                                        \n" +
                "     3          -                                                                                                        \n" +
                "     2          -                                                                                                        \n" +
                "     1          -                                                                                                        \n" +
                "     0          -          -          -          -          -          -          -          -          -          -     \n" +
                "             [1-11]     (11-22]    (22-33]    (33-44]    (44-55]    (55-66]    (66-77]    (77-88]    (88-99]   (99-110]  ";
        String actual = outContent.toString().replace("\r", "");
        Assert.assertTrue(actual.contains(expected));
    }

    @Test
    public void printHistogramNoWordCount() {
        CountedFileContainer container = new CountedFileContainer();

        int numOfFiles = 10;
        while (numOfFiles > 0) {
            File file = TestHelperUtil.createFile(
                    TestHelperUtil.generateWord(5) + ".txt",
                    " ",
                    testDirParent
            );
            container.add(new CountedFile(file));
            numOfFiles--;
        }

        container.printHistogram();

        String expected = "   10        -                                        \n" +
                "    9        -                                        \n" +
                "    8        -                                        \n" +
                "    7        -                                        \n" +
                "    6        -                                        \n" +
                "    5        -                                        \n" +
                "    4        -                                        \n" +
                "    3        -                                        \n" +
                "    2        -                                        \n" +
                "    1        -                                        \n" +
                "    0        -        -        -        -        -    \n" +
                "           [0-5]   (5-11]   (11-17]  (17-23]  (23-29] ";
        String actual = outContent.toString().replace("\r", "");
        Assert.assertTrue(actual.contains(expected));
    }

    @Test
    public void printHistogramMaxLtTotalBuckets() {
        CountedFileContainer container = new CountedFileContainer();

        File file24 = TestHelperUtil.createFile(
                TestHelperUtil.generateWord(5) + ".txt",
                TestHelperUtil.generateRandomString(24),
                testDirParent
        );
        File file23 = TestHelperUtil.createFile(
                TestHelperUtil.generateWord(5) + ".txt",
                TestHelperUtil.generateRandomString(23),
                testDirParent
        );

        container.add(new CountedFile(file24));
        container.add(new CountedFile(file23));

        container.printHistogram();

        String expected = "    2        -                                        \n" +
                "    1        -                                        \n" +
                "    0        -        -        -        -        -    \n" +
                "          [23-28]  (28-34]  (34-40]  (40-46]  (46-52] ";
        String actual = outContent.toString().replace("\r", "");
        Assert.assertTrue(actual.contains(expected));
    }

    @Test
    public void printHistogramOneFile() {
        CountedFileContainer container = new CountedFileContainer();

        int numOfFiles = 1;
        while (numOfFiles > 0) {
            File file = TestHelperUtil.createFile(
                    TestHelperUtil.generateWord(5) + ".txt",
                    TestHelperUtil.generateRandomString(5),
                    testDirParent
            );
            container.add(new CountedFile(file));
            numOfFiles--;
        }

        container.printHistogram();

        String expected = "    1                          -                      \n" +
                "    0        -        -        -        -        -    \n" +
                "           [0-1]    (1-3]    (3-5]    (5-7]    (7-9]  ";
        String actual = outContent.toString().replace("\r", "");
        Assert.assertTrue(actual.contains(expected));
    }
}
