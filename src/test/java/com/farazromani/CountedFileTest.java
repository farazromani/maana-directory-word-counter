package com.farazromani;

import com.farazromani.TestHelper.TestHelperUtil;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class CountedFileTest {

    private final String TEST_DIR_PARENT = "." + File.separator + "testDir_" + System.currentTimeMillis();
    private File testDirParent;

    @Before
    public void createDirAndFile() {
        testDirParent = new File(TEST_DIR_PARENT);
        testDirParent.mkdirs();
    }

    @After
    public void deleteTestDir() throws IOException {
        FileUtils.deleteDirectory(testDirParent);
    }

    @Test
    public void testCountedFileWithWords() {
        final String PHRASE = "this is a hyphen-ated string";
        final int WORD_COUNT = 5;
        File file = TestHelperUtil.createFile("file01", PHRASE, testDirParent);
        CountedFile countedFile = new CountedFile(file);

        Assert.assertNotNull(countedFile);
        Assert.assertEquals(countedFile.getWordCount(), WORD_COUNT);
    }

    @Test
    public void testCountedFileWithNoWords() {
        File file = TestHelperUtil.createFile("file01"," ", testDirParent);
        CountedFile countedFile = new CountedFile(file);

        Assert.assertNotNull(countedFile);
        Assert.assertEquals(countedFile.getWordCount(), 0);
    }

    @Test
    public void testCountedFileWithTrailingSpace() {
        File file = TestHelperUtil.createFile("file01","hello\n \n \n ", testDirParent);
        CountedFile countedFile = new CountedFile(file);

        Assert.assertNotNull(countedFile);
        Assert.assertEquals(countedFile.getWordCount(), 1);
    }

    @Test
    public void testCountedFileWithLeadingSpace() {
        File file = TestHelperUtil.createFile("file01"," \n \n \n \n hello", testDirParent);
        CountedFile countedFile = new CountedFile(file);

        Assert.assertNotNull(countedFile);
        Assert.assertEquals(countedFile.getWordCount(), 1);
    }

    @Test
    public void testCountedFileWithLeadingAndTrailingSpace() {
        File file = TestHelperUtil.createFile("file01"," \n \t \n hello\n \t \n ", testDirParent);
        CountedFile countedFile = new CountedFile(file);

        Assert.assertNotNull(countedFile);
        Assert.assertEquals(countedFile.getWordCount(), 1);
    }
}
