package com.farazromani;

import com.farazromani.Utils.DirectorySearch;
import com.farazromani.Utils.ExceptionHandlerUtil;

import java.io.File;

public class Main {

    public static void main(String[] args) {
        // check if filepath was passed as argument
        if (args == null || args.length == 0) {
            ExceptionHandlerUtil.InvalidArguments();
        } else {

            // get path from arguments
            String path = args[0];

            // create file obj and check if valid
            File file = new File(path);
            boolean exists = file.exists();

            if (exists) {

                // start directory traversal
                CountedFileContainer countedFileContainer = DirectorySearch.Search(file);

                // produce histogram from processed text files
                countedFileContainer.printHistogram();

            } else {
                ExceptionHandlerUtil.NoSuchDirectory(file);
            }
        }
    }
}
