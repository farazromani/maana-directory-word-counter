package com.farazromani;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        MainTest.class,
        CountedFileTest.class,
        CounterFileContainerTest.class,
        DecompressUtilTest.class,
        ExceptionHandlerUtilTest.class,
        HistogramUtilTest.class,
        DirectorySearchTest.class
})

public class SuiteTest {
}
