package com.farazromani.Utils;

import com.farazromani.CountedFile;
import com.farazromani.CountedFileContainer;
import org.apache.commons.lang.StringUtils;

import java.util.LinkedList;

/**
 * This method will output a histogram to the console. Given a container containing ContentFiles, it will automatically
 * figure out the number of buckets needed, the size of each bucket, and which ContentFile belongs in which bucket.
 */
public class HistogramUtil {
    private static HistogramUtil ourInstance = new HistogramUtil();

    private HistogramUtil() {
    }

    public static HistogramUtil getInstance() {
        return ourInstance;
    }

    /**
     * Print histogram representation of @code{fileContainer}
     *
     * @param fileContainer container of ContentFiles to be parsed and histogram-ified
     */
    public static void PrintHistogram(CountedFileContainer fileContainer) {
        final int TOTAL_BUCKETS = getTotalBuckets(fileContainer);
        final int BUCKET_INCREMENT_SIZE = getBucketIncrementSize(fileContainer, TOTAL_BUCKETS);

        // convert fileCounter's word counts into frequencies array
        int[] frequencies = getFreqFromContainer(fileContainer, TOTAL_BUCKETS, BUCKET_INCREMENT_SIZE);

        // get max frequency to determine y-axis increments
        int maxFrequency = Integer.MIN_VALUE;
        int minFrequency = Integer.MAX_VALUE;
        for (int freq : frequencies) {
            maxFrequency = Math.max(maxFrequency, freq);
            minFrequency = Math.min(minFrequency, freq);
        }

        // y-axis interval
        final int FREQ_INTERVALS = Math.min(10, maxFrequency);

        // how big each frequency interval is
        final double FREQ_INCREMENT_SIZE = (double) maxFrequency / (double) FREQ_INTERVALS;

        // width of the column on histogram
        final int COLUMN_LABEL_WIDTH = getColumnLabelWidth(BUCKET_INCREMENT_SIZE) + 2;

        String[] histogramRow = new String[TOTAL_BUCKETS + 1];
        for (int row = FREQ_INTERVALS; row >= 0; row--) {
            for (int col = histogramRow.length - 1; col >= 0; col--) {
                String histogramVal;

                // create y-axis labels
                if (col == 0) {
                    double xLabel = Math.ceil(row * FREQ_INCREMENT_SIZE);
                    histogramVal = StringUtils.center(Integer.toString((int) xLabel), COLUMN_LABEL_WIDTH);
                }

                // place tick-mark within histogram if word count falls within frequency
                else {
                    // current frequency to place on histogram
                    int frequency = frequencies[col - 1];

                    // ensure frequency within frequency interval
                    int adjFrequency = (int) Math.floor(frequency / FREQ_INCREMENT_SIZE);

                    // frequency falls within frequency interval -- mark histogram
                    if (adjFrequency >= row) {
                        histogramVal = StringUtils.center("-", COLUMN_LABEL_WIDTH);
                    }

                    // frequency falls outside of frequency interval -- leave blank
                    else {
                        histogramVal = StringUtils.center(" ", COLUMN_LABEL_WIDTH);
                    }
                }

                // insert value within correct location on histogram
                histogramRow[col] = histogramVal;
            }

            // print cur row's values by column
            for (int i = 0; i <= histogramRow.length; i++) {

                // add empty line after end of traversal
                if (i == histogramRow.length) {
                    System.out.println();
                }

                // print current value on same line
                else {
                    System.out.print(histogramRow[i]);
                }
            }
        }

        // print X-axis labels
        printXAxisLabels(fileContainer, TOTAL_BUCKETS, BUCKET_INCREMENT_SIZE, COLUMN_LABEL_WIDTH);
        printContainerInfo(fileContainer);

//        printDebugInfo(frequencies, fileContainer, BUCKET_INCREMENT_SIZE, TOTAL_BUCKETS);
    }

    /**
     * Will print each buckets bounds are part of the x-axis
     *
     * @param fileContainer    container for ContentFiles
     * @param totalBuckets     total number of buckets in the histogram
     * @param bucketSize       the size of each bucket
     * @param columnLabelWidth how wide the label should be for proper text alignment
     */
    private static void printXAxisLabels(CountedFileContainer fileContainer, int totalBuckets, int bucketSize, int columnLabelWidth) {
        // calculate right-most bucket limits per bucket
        int[] freqIntervals = new int[totalBuckets + 1];
        for (int i = 0; i < freqIntervals.length; i++) {
            if (i == 0) {
                freqIntervals[i] = (fileContainer.getMinWordCount() == fileContainer.getMaxWordCount())
                        ? bucketSize
                        : fileContainer.getMinWordCount() + bucketSize;
            } else {
                freqIntervals[i] = (freqIntervals[i - 1] + 1) + bucketSize;
            }

            // print labels
            if (i == 0) {
                System.out.print(StringUtils.center(" ", columnLabelWidth));
            } else {
                System.out.print((i == 1)
                        ? StringUtils.center("[" + (freqIntervals[i - 1] - bucketSize) + "-" + (freqIntervals[i] - bucketSize - 1) + "]", columnLabelWidth)
                        : StringUtils.center("(" + (freqIntervals[i - 1] - bucketSize - 1) + "-" + (freqIntervals[i] - bucketSize - 1) + "]", columnLabelWidth)
                );
            }
        }
    }

    /**
     * Will return the number of buckets/columns the histogram should have. Ranges from 5 to 20.
     *
     * @param countContainer list of WordCounts.
     * @return number of buckets/columns the histogram should have.
     */
    private static int getTotalBuckets(CountedFileContainer countContainer) {
        int minCount = countContainer.getMinWordCount();
        int maxCount = countContainer.getMaxWordCount();
        int range = maxCount - minCount;

        int buckets = 5;
        int maxBuckets = 20;
        while (buckets <= maxBuckets) {
            int quotient = (int) Math.ceil(range / buckets);
            if (quotient % 2 == 0) break;
            buckets++;
        }

        return buckets;
    }

    /**
     * Returns an array that is @code{totalBuckets} in length. It will iterator through all the ContentFiles within the
     *
     * @param fileContainer container of CountedFiles
     * @param totalBuckets  total number of buckets in the histogram
     * @param bucketSize    how big each bucket is
     * @return int array representing how many CountedFile files within each bucket
     * @code{fileContainer} and increment the proper array index.
     */
    private static int[] getFreqFromContainer(CountedFileContainer fileContainer, int totalBuckets, int bucketSize) {
        // counts how many CountedFiles fits in each bucket
        int[] frequencies = new int[totalBuckets];

        // tracks the right-limit of each bucket
        int[] freqIntervalLimit = new int[totalBuckets];

        // traverse arr and calculate right-hand bucket size limit per bucket
        for (int i = 0; i < freqIntervalLimit.length; i++) {
            // special case for first bucket
            if (i == 0) {
                freqIntervalLimit[i] = (fileContainer.getMinWordCount() == fileContainer.getMaxWordCount())
                        ? bucketSize
                        : fileContainer.getMinWordCount() + bucketSize;
            } else {
                freqIntervalLimit[i] = (freqIntervalLimit[i - 1] + 1) + bucketSize;
            }
        }

        // iterate through container and increment correct frequencies arr index depending on word count
        for (CountedFile countedFile : fileContainer.getList()) {
            int wordCount = countedFile.getWordCount();

            // find which index current CountedFile belongs
            int freqIndex = 0;
            while (freqIndex < totalBuckets) {
                if (wordCount <= freqIntervalLimit[freqIndex]) break;
                freqIndex++;
            }

            // keep within bounds
            if (freqIndex >= totalBuckets) freqIndex = totalBuckets - 1;

            // increment number of frequency at index
            frequencies[freqIndex]++;
        }

        return frequencies;
    }

    /**
     * Return how big each bucket should be for the histogram.
     *
     * @param countContainer container for CountedFiles
     * @param totalBuckets   total number of buckets the histogram has
     * @return int representing the size of each bucket for the histogram
     */
    private static int getBucketIncrementSize(CountedFileContainer countContainer, int totalBuckets) {
        int minCount = countContainer.getMinWordCount();
        int maxCount = countContainer.getMaxWordCount();

        if (minCount == maxCount) {
            if (minCount < totalBuckets) {
                return totalBuckets;
            }
            return (int) Math.ceil(minCount / totalBuckets);

        } else {
            double diff = maxCount - minCount;
            if (diff < totalBuckets) {
                return totalBuckets;
            }

            return (int) Math.ceil(diff / (double) totalBuckets);
        }
    }

    /**
     * Width of the column. Helps with printing X-axis labels
     *
     * @param bucketSize how big each bucket is within the histogram
     * @return int representing the length of each column within the histogram
     */
    private static int getColumnLabelWidth(int bucketSize) {
        String tmpLabel = "(" + bucketSize + " - " + bucketSize + ")".length();
        return tmpLabel.length();
    }

    private static void printContainerInfo(CountedFileContainer fileContainer) {
        LinkedList<CountedFile> files = fileContainer.getList();

        final String indexHeading = "NO.";
        final String absPathHeading = "ABSOLUTE PATH";
        final String wordCountHeading = "WORD COUNT";

        final int MAX_TOTAL_COUNT_NUM_LEN = Math.max(indexHeading.length(), String.valueOf(files.size()).length()) + 3;
        final int MAX_ABSOLUTE_FILENAME_LEN = Math.max(absPathHeading.length(), fileContainer.getMaxAbsolutePathLen()) + 3;
        final int MAX_WORDCOUNT_NUM_LEN = Math.max(wordCountHeading.length(), String.valueOf(fileContainer.getMaxWordCount()).length()) + 3;

        final char PADDING = ' ';

        String heading = StringUtils.center("\n\n" + indexHeading, MAX_TOTAL_COUNT_NUM_LEN, PADDING)
                + StringUtils.center(absPathHeading, MAX_ABSOLUTE_FILENAME_LEN, PADDING)
                + StringUtils.leftPad(wordCountHeading, MAX_WORDCOUNT_NUM_LEN, PADDING);
        System.out.println(heading);

        for (int i = 0; i < files.size(); i++) {
            CountedFile file = files.get(i);

            String index = StringUtils.rightPad(String.valueOf(i + 1), MAX_TOTAL_COUNT_NUM_LEN, ' ');
            String filename = StringUtils.rightPad(file.getFile().getAbsolutePath(), MAX_ABSOLUTE_FILENAME_LEN, ' ');
            String count = StringUtils.center(String.valueOf(file.getWordCount()), MAX_WORDCOUNT_NUM_LEN, ' ');

            System.out.println(index + filename + count);
        }

    }

//    private static void printDebugInfo(int[] frequencies, CountedFileContainer fileContainer, int bucketSize, int totalBuckets) {
//        System.out.println("\nPRINTING DEBUG INFO...");
//        System.out.println("\nTOTAL BUCKETS:\t" + totalBuckets);
//        System.out.println("\nBUCKET SIZE:\t" + bucketSize);
//        System.out.println("\nFREQUENCIES:\t" + Arrays.toString(frequencies) + "\n");
//
//        final int MAX_WORDCOUNT_NUM_LEN = String.valueOf(fileContainer.getMaxWordCount()).length() + 3;
//        final int MAX_ABSOLUTE_FILENAME_LEN = fileContainer.getMaxAbsolutePathLen() + 3;
//
//        LinkedList<CountedFile> countedFiles = fileContainer.getList();
//        for (int i = 0; i < countedFiles.size(); i++) {
//            CountedFile file = countedFiles.get(i);
//            String filename = StringUtils.left(file.getFile().getAbsolutePath(), MAX_ABSOLUTE_FILENAME_LEN);
//            String count = StringUtils.left(String.valueOf(file.getWordCount()), MAX_WORDCOUNT_NUM_LEN);
//            System.out.println(i + "\t" + filename + "\t" + count);
//        }
//    }
}
