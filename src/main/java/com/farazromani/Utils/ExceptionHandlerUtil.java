package com.farazromani.Utils;

import com.farazromani.Enums.ErrorCode;

import java.io.File;

import static com.farazromani.Enums.ErrorCode.*;

public class ExceptionHandlerUtil {
    private static ExceptionHandlerUtil ourInstance = new ExceptionHandlerUtil();

    private ExceptionHandlerUtil() {
    }

    public static ExceptionHandlerUtil getInstance() {
        return ourInstance;
    }

    public static void InvalidArguments() {
        HandleException(null, null, MISSING_PATH_ENV, true);
    }

    public static void NoSuchDirectory(File invalidFile) {
        HandleException(null, invalidFile, INVALID_PATH, true);
    }

    public static void NoTextFiles() {
        HandleException(null, null, NO_TEXT_FILES_PARSED, false);
    }

    public static void AccessDenied(File file) {
        HandleException(null, file, ACCESS_DENIED, true);
    }

    public static void UnknownError() {
        HandleException(null, null, UNKNOWN_PARSING_ERROR, true);
    }

    /**
     * Will produce an error msg during an Java-generated exception with unknown source
     */
    public static void HandleException(Exception e, File file) {
        HandleException(e, file, null, true);
    }

    /**
     * Helper method to display error message depending on type of @code{ErrorCode} passed in.
     */
    private static void HandleException(Exception e, File file, ErrorCode errorCode, boolean terminal) {
        StringBuilder msg = new StringBuilder("ERROR");

        if (file != null) {
            msg.append(":\tCannot process '")
                    .append(file.getName())
                    .append("' (")
                    .append(file.getAbsolutePath())
                    .append(")");
        }

        msg.append("\nREASON:\t");

        if (e != null) {
            String errMsg = e.getMessage();
            if (errMsg != null && errMsg.contains("(Access is denied)")) {
                errorCode = ACCESS_DENIED;
            }
        }

        if (errorCode == null) {
            msg.append(UNKNOWN_PARSING_ERROR);
        } else {
            switch (errorCode) {
                case INVALID_PATH:
                    msg.append(INVALID_PATH);
                    break;
                case MISSING_PATH_ENV:
                    msg.append(MISSING_PATH_ENV);
                    break;
                case ACCESS_DENIED:
                    msg.append(ACCESS_DENIED);
                    break;
                case NO_TEXT_FILES_PARSED:
                    msg.append(NO_TEXT_FILES_PARSED);
                    break;
                default:
                    msg.append(UNKNOWN_PARSING_ERROR);
                    break;
            }
        }

        System.out.println(msg.toString());

        if (terminal) {
            System.exit(-1);
        }
    }
}
