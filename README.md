# Maana Directory Text File Word Counter

## Description
This program will recursively search through a given directory, find all text files (`.txt`), and keep track of their word count. Once the search is complete, a histogram will be generated and outputted to the console which will represent the total number of words found in each text file.

The program also has support for compressed directories. Currently, it has been tested for only `.zip`, `.tar`, and `.tar.gz` compression types.

## Running the Program
You can run the program using Command Line / Terminal.

```bash
java -jar directory-word-counter.java <path>
```

Where `<path>` can be either relative or absolute. For example:

```bash
java -jar directory-word-counter.java . 							# relative
java -jar directory-word-counter.java C:\Users\foobar\Documents		# absolute
```

Here's a sample output:

```
15 total number of text files discovered.
2 total number of directories discovered (incl. root).

    4        -        -        -                               
    3        -        -        -        -                      
    2        -        -        -        -                      
    1        -        -        -        -                      
    0        -        -        -        -        -        -    
           [5-8]   (8-12]   (12-16]  (16-20]  (20-24]  (24-28] 

NO.                                       ABSOLUTE PATH                                         WORD COUNT
1     C:\Users\faraz\Desktop\random-text-files\inral.txt                                             9      
2     C:\Users\faraz\Desktop\random-text-files\iokjq.txt                                             5      
3     C:\Users\faraz\Desktop\random-text-files\iqhdh.txt                                             6      
4     C:\Users\faraz\Desktop\random-text-files\isufd.txt                                             7      
5     C:\Users\faraz\Desktop\random-text-files\itotg.txt                                            18      
6     C:\Users\faraz\Desktop\random-text-files\some-compressed-folder_decompressed\egpdo.txt        13      
7     C:\Users\faraz\Desktop\random-text-files\some-compressed-folder_decompressed\gmxib.txt        20      
8     C:\Users\faraz\Desktop\random-text-files\some-compressed-folder_decompressed\gmyhx.txt        11      
9     C:\Users\faraz\Desktop\random-text-files\some-compressed-folder_decompressed\gqmzl.txt         9      
10    C:\Users\faraz\Desktop\random-text-files\some-compressed-folder_decompressed\gqvsi.txt        14      
11    C:\Users\faraz\Desktop\random-text-files\some-compressed-folder_decompressed\gzvcb.txt        12      
12    C:\Users\faraz\Desktop\random-text-files\some-compressed-folder_decompressed\hcceq.txt        13      
13    C:\Users\faraz\Desktop\random-text-files\some-compressed-folder_decompressed\herno.txt        17      
14    C:\Users\faraz\Desktop\random-text-files\some-compressed-folder_decompressed\hfoci.txt        15      
15    C:\Users\faraz\Desktop\random-text-files\some-compressed-folder_decompressed\hlbpd.txt         7      
```

## Tests

### Running Tests
The test suite was created using JUnit. For simplicity's sake, you should run the test suite (`src\test\java\com\farazromani\SuiteTest.java`) through an IDE, such as Eclipse or Intellij IDEA.

### Coverage and Reports
The test covers the following files:

- `com.farazromani.Main`
- `com.farazromani.CountedFile`
- `com.farazromani.CountedFileContainer`
- `com.farazromani.Enums.ErrorCode`
- `com.farazromani.Enums.Extension`
- `com.farazromani.Utils.DecompressUtil`
- `com.farazromani.Utils.DirectorySearch`
- `com.farazromani.Utils.ExceptionHandlerUtil`
- `com.farazromani.Utils.HistogramUtil`

You can also see latest code coverage report by visiting `test-coverage-report-2017-04-10\index.html` from the root of the project.

### Known Coverage Holes
The following area(s) of testing were skipped:

- **Hidden/Restricted (Compressed) Files and Directories** -- it was proving difficult to dynamically modify the attributes of a file, directory, or compressed file/directory in order to make them either hidden and/or restricted. As a result, unit testing does no cover this test case. However, this _was_ manually tested.

## Building Project
Import the project as a Maven project using your favorite IDE. Intellij IDEA was used for this program's development.

## Error Handling
The program will exit gracefully, with `exit code -1`, if it runs into the following issues while executing:

- No `<path>` passed into Command Line / Terminal
- Path does not exist
- File, directory, or compressed file with permission issues / access denied

> **NOTE:** if no text files are found, the program will display a friendly message but the program will exit on its own (`exit code 0`).

## Limitations & Assumptions
- Text files do not contain more than 2,147,483,647 words (Integer max value).
- Text files contain ASCII characters. Otherwise, non-ASCII might not be count as words.
- Compressed types supported are limited to `.zip`, `.tar`, and `.tar.gz`
- The program does not clean up afterwards. For example, the uncompressed directory is not deleted after program exits.

> **NOTE:** JUnit tests _do_ clean up after themselves -- they delete all dynamically generated files and directories.

- Directory names should not be postfixed with `_decompressed` as it may cause the directory to be overriden if it shares the same name as a zipped file.

## Changelog

### 2017-04-13
- Add total number of files and directories to ouptut.
- Output entire list of `CountedFiles` within `CountedFileContainer`.
- Postfix `_decompressed` to compressed directories upon decompression.
